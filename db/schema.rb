# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140511114545) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "moves", force: true do |t|
    t.integer  "user_id"
    t.integer  "place_id"
    t.integer  "year"
    t.integer  "month"
    t.integer  "mday"
    t.integer  "hour"
    t.integer  "minute"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "timestamp"
  end

  add_index "moves", ["place_id"], name: "index_moves_on_place_id", using: :btree
  add_index "moves", ["user_id"], name: "index_moves_on_user_id", using: :btree

  create_table "places", force: true do |t|
    t.text     "name"
    t.text     "tz_name"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "phone_number"
  end

  add_index "places", ["user_id"], name: "index_places_on_user_id", using: :btree

  create_table "users", force: true do |t|
    t.text     "name"
    t.text     "email_address"
    t.text     "password_digest"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "uuid"
  end

  add_index "users", ["email_address"], name: "index_users_on_email_address", using: :btree
  add_index "users", ["uuid"], name: "index_users_on_uuid", using: :btree

end

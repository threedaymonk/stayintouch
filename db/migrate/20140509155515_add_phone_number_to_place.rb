class AddPhoneNumberToPlace < ActiveRecord::Migration
  def change
    add_column :places, :phone_number, :text
  end
end

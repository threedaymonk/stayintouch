class CreateMoves < ActiveRecord::Migration
  def change
    create_table :moves do |t|
      t.belongs_to :user, index: true
      t.belongs_to :place, index: true
      t.integer :year
      t.integer :month
      t.integer :mday
      t.integer :hour
      t.integer :minute

      t.timestamps
    end
  end
end

class CreatePlaces < ActiveRecord::Migration
  def change
    create_table :places do |t|
      t.belongs_to :user, index: true
      t.text :name
      t.text :tz_name

      t.timestamps
    end
  end
end

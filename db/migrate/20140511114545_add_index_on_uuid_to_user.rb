class AddIndexOnUuidToUser < ActiveRecord::Migration
  def change
    add_index :users, :uuid, length: 36 # 32 + four hyphens: 8-4-4-4-12
  end
end

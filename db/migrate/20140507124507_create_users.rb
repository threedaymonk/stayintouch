class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.text :name
      t.text :email_address
      t.text :password_digest

      t.index :email_address, length: 20

      t.timestamps
    end
  end
end

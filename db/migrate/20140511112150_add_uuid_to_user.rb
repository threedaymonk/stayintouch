class AddUuidToUser < ActiveRecord::Migration
  class User < ActiveRecord::Base
  end

  def change
    add_column :users, :uuid, :string
    User.all.each do |user|
      user.uuid = SimpleUUID::UUID.new.to_guid
      user.save!
    end
  end
end

class AddTimestampToMove < ActiveRecord::Migration
  def change
    add_column :moves, :timestamp, :integer
  end
end

class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper_method :current_user, :current_place

protected
  def current_user
    @current_user ||= User.find_by_id(session[:user_id])
  end

  def current_place
    current_user && current_user.current_place
  end

  def require_current_user
    redirect_to new_authentication_path unless current_user
  end

  def redirect_by_goal
    redirect_to root_url
  end
end

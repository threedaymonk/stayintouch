class MovesController < ApplicationController
  before_action :require_current_user
  before_action :set_move, only: [:show, :edit, :update, :destroy]

  # GET /moves
  def index
    @moves = moves.chronologically
  end

  # GET /moves/new
  def new
    @move = moves.new
  end

  # GET /moves/1/edit
  def edit
  end

  # POST /moves
  def create
    @move = moves.new(move_params)

    if @move.save
      redirect_to moves_path, notice: 'Move was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /moves/1
  def update
    if @move.update(move_params)
      redirect_to moves_path, notice: 'Move was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /moves/1
  def destroy
    @move.destroy
    redirect_to moves_path, notice: 'Move was successfully destroyed.'
  end

private
  def moves
    @current_user.moves
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_move
    @move = moves.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def move_params
    params.require(:move).permit(:user_id, :place_id, :year, :month, :mday, :hour, :minute)
  end
end

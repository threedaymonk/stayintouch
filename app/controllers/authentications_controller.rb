class AuthenticationsController < ApplicationController
  # GET /authentication/new
  def new
    @authentication = Authentication.new
  end

  # POST /authentication
  def create
    @authentication = Authentication.new(authentication_params)

    if @authentication.save
      session[:user_id] = @authentication.user.id
      request.session_options[:renew] = true

      flash[:notice] = 'Signed in'
      redirect_by_goal
    else
      render :new
    end
  end

  # DELETE /authentication
  def destroy
    reset_session
    redirect_to root_url, notice: 'Signed out'
  end

private
  def authentication_params
    params.require(:authentication).permit(:email_address, :password)
  end
end

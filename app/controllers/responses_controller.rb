class ResponsesController < ApplicationController
  def show
    @user = User.find_by_uuid(params[:id])
  end
end

class PlacesController < ApplicationController
  before_action :require_current_user
  before_action :set_place, only: [:show, :edit, :update, :destroy]

  # GET /places
  def index
    @places = places.alphabetically
  end

  # GET /places/new
  def new
    @place = places.new
  end

  # GET /places/1/edit
  def edit
  end

  # POST /places
  def create
    @place = places.new(place_params)

    if @place.save
      redirect_to places_url, notice: 'Place was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /places/1
  def update
    if @place.update(place_params)
      redirect_to places_url, notice: 'Place was successfully updated.'
    else
      render :edit
    end
  end

private
  def places
    @current_user.places
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_place
    @place = places.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def place_params
    params.require(:place).permit(:name, :tz_name, :phone_number)
  end
end

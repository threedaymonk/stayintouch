xml.instruct!
xml.Response {
  if @user.current_phone_number
    xml.Dial @user.current_phone_number, action: "/forward?dial=true", timeout: 20
  else
    xml.Say "This phone number is not yet configured"
  end
}


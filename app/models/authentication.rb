class Authentication
  include ActiveModel::Conversion
  extend ActiveModel::Naming

  attr_accessor :email_address, :password
  attr_reader :user

  def to_key
    [ email_address ]
  end

  def initialize(opts = {})
    @email_address = opts[:email_address]
    @password = opts[:password]
  end

  def save
    @user = User.authenticate(email_address, password)
    !!@user
  end

  def persisted?
    false
  end
end

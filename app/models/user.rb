require "bcrypt"
require "forwardable"

class User < ActiveRecord::Base
  extend Forwardable
  attr_reader :password

  validates_presence_of :email_address
  validates_format_of :email_address, with: /.@./
  validates_presence_of :password, on: :create
  validates_length_of :password,
    minimum: 8,
    if: lambda { |u| u.password.present? }
  validates_uniqueness_of :email_address

  has_many :places
  has_many :moves

  def_delegator :moves, :current_place, :current_place

  after_initialize :set_uuid

  def self.authenticate(email_address, password)
    user = find_by_email_address(email_address)
    if user && user.valid_password?(password)
      user
    else
      nil
    end
  end

  def password=(password)
    return if password.blank?
    @password = password
    self.password_digest = BCrypt::Password.create(password)
  end

  def valid_password?(password)
    return false unless password_digest.present?
    BCrypt::Password.new(password_digest) == password
  end

  def to_s
    [name, email_address].reject(&:blank?).first
  end

  def set_uuid
    self.uuid ||= SimpleUUID::UUID.new.to_guid
  end

  def current_phone_number
    current_place && current_place.normalized_phone_number
  end
end

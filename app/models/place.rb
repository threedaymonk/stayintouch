require "tzinfo"

class Place < ActiveRecord::Base
  def self.zone_identifiers
    TZInfo::Timezone.all_country_zone_identifiers.sort
  end

  validates_inclusion_of :tz_name, in: zone_identifiers
  validates_format_of :phone_number, with: /\A\+[0-9\-. ()]+\z/
  belongs_to :user
  has_many :moves

  def self.alphabetically
    order(name: :asc)
  end

  def utc(year, month, mday, hour, minute, second = 0)
    TZInfo::Timezone.get(tz_name).local_to_utc(
      DateTime.new(year, month, mday, hour, minute, second))
  end

  def to_s
    [name, tz_name].reject(&:blank?).first
  end

  def after_save
    moves.each do |move|
      move.compute_timestamp
      move.save
    end
  end

  def normalized_phone_number
    phone_number.gsub(/[^\+0-9]/, "")
  end
end

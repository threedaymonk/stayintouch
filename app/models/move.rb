require "forwardable"

class Move < ActiveRecord::Base
  extend Forwardable
  belongs_to :user
  belongs_to :place
  validates :user, presence: true
  validates :place, presence: true
  validates :year, numericality: {
    only_integer: true,
    greater_than_or_equal_to: 2000,
    less_than_or_equal_to: 2100
  }
  validates :month, numericality: {
    only_integer: true,
    greater_than_or_equal_to: 1,
    less_than_or_equal_to: 12
  }
  validates :mday, numericality: {
    only_integer: true,
    greater_than_or_equal_to: 1,
    less_than_or_equal_to: 31
  }
  validates :hour, numericality: {
    only_integer: true,
    greater_than_or_equal_to: 0,
    less_than_or_equal_to: 23
  }
  validates :minute, numericality: {
    only_integer: true,
    greater_than_or_equal_to: 0,
    less_than_or_equal_to: 59
  }

  validate do |move|
    unless place.user == move.user
      move.errors.add(:base, "Invalid place")
    end
  end


  def_delegator :place, :name, :place_name

  before_save :compute_timestamp

  def self.chronologically
    order(timestamp: :asc)
  end

  def self.current_place(now = Time.now)
    move = order(timestamp: :desc).
      where(arel_table[:timestamp].lteq(now.to_i)).
      first
    move && move.place
  end

  def date=(ymd)
    if (m = ymd.match(/\A(\d{4})-(\d{2})-(\d{2})\z/))
      self.year, self.month, self.mday = [1, 2, 3].map { |i| m[i].to_i }
    end
  end

  def date
    "%04d-%02d-%02d" % [year, month, mday]
  end

  def time=(hm)
    if (m = hm.match(/\A(\d{2}):(\d{2})\z/))
      self.hour, self.minute = [1, 2, 3].map { |i| m[i].to_i }
    end
  end

  def time
    "%02d:%02d" % [hour, minute]
  end

  def local_date_string
    "#{date} #{time}"
  end

  def utc_date_string
    u = self.utc
    "%04d-%02d-%02d %02d:%02d" % [u.year, u.month, u.mday, u.hour, u.minute]
  end

  def utc
    place.utc(year, month, mday, hour, minute)
  end

  def compute_timestamp
    self.timestamp = self.utc.to_i
  end

  def timestamp
    read_attribute(:timestamp) || compute_timestamp
  end
end

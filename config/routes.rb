Rails.application.routes.draw do
  root to: "home#index"
  resource :authentication, only: [:new, :create, :destroy]
  resources :moves, only: [:index, :new, :edit, :create, :update, :destroy]
  resources :places, only: [:index, :new, :edit, :create, :update]
  match "responses/:id", to: "responses#show", via: [:get, :post], as: :response
  resource :user, only: [:new, :create, :show, :edit, :update]
end

module Support
  module Integration
    def signed_in_user
      name = "Bob McUser"
      email_address = "user@example.com"
      password = "secure11!!"

      user = create(
        :user,
        name: name,
        email_address: email_address,
        password: password
      )

      visit new_authentication_path

      fill_in "Email address", with: email_address
      fill_in "Password", with: password
      click_button "Sign in"

      return user
    end
  end
end

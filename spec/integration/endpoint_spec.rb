require_relative "../spec_helper"

feature "Endpoint" do
  scenario "message when there is no current place" do
    user = create(:user)
    get "/responses/#{user.uuid}"
    doc = Nokogiri::XML(response.body)
    expect(doc.at("/Response/Dial")).to be_nil
    expect(doc.at("/Response/Say").text).to match(/not yet configured/)
  end

  scenario "forwarding a call when there is a current place" do
    user = create(:user)
    place = create(
      :place,
      user: user,
      phone_number: "+1 (555) 123-4567"
    )
    move = create(
      :move,
      user: user,
      place: place,
      year: 2012
    )
    get "/responses/#{user.uuid}"
    doc = Nokogiri::XML(response.body)
    expect(doc.at("/Response/Say")).to be_nil
    expect(doc.at("/Response/Dial").text).to eql("+15551234567")
  end

  scenario "responding to POST" do
    user = create(:user)
    post "/responses/#{user.uuid}"
    doc = Nokogiri::XML(response.body)
    expect(doc.at("/Response")).not_to be_nil
  end
end

require_relative "../spec_helper"

feature "Places" do
  scenario "Adding a place" do
    user = signed_in_user

    visit places_path
    click_link "Add a place"

    fill_in "Name", with: "Japan"
    select "Asia/Tokyo", from: "Time zone"
    fill_in "Phone number", with: "+1 (555) 123-4567"
    click_button "Create Place"

    place = user.places.first
    expect(place.name).to eql("Japan")
    expect(place.tz_name).to eql("Asia/Tokyo")
  end
end

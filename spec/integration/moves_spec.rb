require_relative "../spec_helper"

feature "Moves" do
  scenario "Adding a move" do
    user = signed_in_user
    place = user.places.create(
      name: "Japan", tz_name: "Asia/Tokyo", phone_number: "+1 (555) 123-4567"
    )

    visit moves_path
    click_link "Add travel"

    select "Japan", from: "Destination"
    select "2014", from: "Year"
    select "5", from: "Month"
    select "9", from: "Day of month"
    select "10", from: "Hour"
    select "30", from: "Minute"

    click_button "Create travel"

    move = user.moves.first
    expect(move.place_name).to eql("Japan")
    expect(move.local_date_string).to eql("2014-05-09 10:30")
  end
end


require_relative "../spec_helper"

feature "Accounts" do
  NAME = "Bob McUser"
  EMAIL_ADDRESS = "user@example.com"
  PASSWORD = "secure11!!"

  scenario "Creating an account" do
    visit new_user_path

    fill_in "Email address", with: EMAIL_ADDRESS
    fill_in "Password", with: PASSWORD
    click_button "Sign up"

    expect(User.find_by_email_address(EMAIL_ADDRESS)).not_to be_nil
  end

  scenario "Signing in and out" do
    create(:user, name: NAME, email_address: EMAIL_ADDRESS, password: PASSWORD)

    visit new_authentication_path

    fill_in "Email address", with: EMAIL_ADDRESS
    fill_in "Password", with: PASSWORD
    click_button "Sign in"

    expect(page).to have_text(NAME)

    click_button "Sign out"

    expect(page).to have_no_text(NAME)
  end
end

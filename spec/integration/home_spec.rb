require_relative "../spec_helper"

feature "Home" do
  scenario "Not signed in" do
    visit "/"
    expect(page).to have_text("Sign in or sign up to get started.")
  end

  scenario "Signed in without a current place" do
    user = signed_in_user
    visit "/"
    expect(page).to have_text("Hello #{user.name}")
  end

  scenario "Signed in with a current place" do
    user = signed_in_user
    place = create(:place, user: user, name: "Novosibirsk")
    move = create(:move, place: place, user: user, year: 2012)
    visit "/"
    expect(page).to have_text("You’re currently in Novosibirsk")
    expect(page).to have_text("http://www.example.com/responses/#{user.uuid}")
  end
end

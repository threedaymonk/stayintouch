require_relative "../spec_helper"

describe "Place" do
  it "should enumerate zone identifiers" do
    expect(Place.zone_identifiers).to include("Europe/London")
  end

  it "should convert a time based on its zone" do
    place = Place.new(tz_name: "Asia/Tokyo")
    utc = place.utc(2014, 5, 9, 22, 30)
    expect(utc.year).to eql(2014)
    expect(utc.month).to eql(5)
    expect(utc.mday).to eql(9)
    expect(utc.hour).to eql(13)
    expect(utc.minute).to eql(30)
    expect(utc.second).to eql(0)
  end

  it "should normalize phone number by removing formatting" do
    place = Place.new(phone_number: "+81-90-0000-1111")
    expect(place.normalized_phone_number).to eql("+819000001111")
  end
end

require_relative "../spec_helper"

describe "Move" do
  it "should build local time string" do
    move = Move.new(
      year: 2014,
      month: 5,
      mday: 9,
      hour: 10,
      minute: 30
    )
    expect(move.local_date_string).to eq("2014-05-09 10:30")
  end

  it "should build UTC time string using place" do
    place = Place.new(tz_name: "Asia/Tokyo")
    move = Move.new(
      place: place,
      year: 2014,
      month: 5,
      mday: 9,
      hour: 10,
      minute: 30
    )
    expect(move.utc_date_string).to eq("2014-05-09 01:30")
  end

  it "should compute UTC timestamp" do
    place = Place.new(tz_name: "Asia/Tokyo")
    move = Move.new(
      place: place,
      year: 2014,
      month: 5,
      mday: 9,
      hour: 10,
      minute: 30
    )
    move.compute_timestamp
    expect(move.timestamp).to eq(1399599000)
  end

  it "should set YYYY-MM-DD date" do
    move = Move.new(date: "2013-09-02")
    expect(move.year).to eql(2013)
    expect(move.month).to eql(9)
    expect(move.mday).to eql(2)
  end

  it "should get YYYY-MM-DD date" do
    move = Move.new(year: 2013, month: 9, mday: 2)
    expect(move.date).to eql("2013-09-02")
  end

  it "should set 24-hour HH:MM time" do
    move = Move.new(time: "13:45")
    expect(move.hour).to eql(13)
    expect(move.minute).to eql(45)
  end

  it "should set 24-hour HH:MM time" do
    move = Move.new(hour: 13, minute: 45)
    expect(move.time).to eql("13:45")
  end
end

describe "Move collection" do
  before(:each) do
    @user = create(:user)
    @uk = create(:place, user: @user, name: "UK", tz_name: "Europe/London")
    @japan = create(:place, user: @user, name: "Japan", tz_name: "Asia/Tokyo")
    @france = create(:place, user: @user, name: "France", tz_name: "Europe/Paris")
  end

  it "should return nil given no moves" do
    expect(Move.current_place).to be_nil
  end

  it "should return last location before timestamp" do
    create(
      :move,
      user: @user, place: @japan,
      date: "2013-05-10", time: "12:30"
    )
    create(
      :move,
      user: @user, place: @uk,
      date: "2013-05-10", time: "12:30"
    )
    create(
      :move,
      user: @user, place: @france,
      date: "2013-06-10", time: "12:30"
    )
    now = Time.utc(2013, 5, 10, 15)
    expect(Move.current_place(now)).to eql(@uk)
  end
end

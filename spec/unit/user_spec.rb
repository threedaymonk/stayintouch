require_relative "../spec_helper"

describe "User" do
  it "should store a digest of the password" do
    user = User.new
    user.password = "hackedbynsa"
    expect(user.password_digest).to match(/.{20,}/)
  end

  it "should not change password_digest for empty password" do
    user = User.new
    user.password = "hackedbynsa"
    old_digest = user.password_digest.dup
    user.password = ""
    expect(user.password_digest).to eql(old_digest)
  end

  it "should accept correct password" do
    user = User.new
    user.password = "hackedbynsa"
    expect(user).to be_valid_password("hackedbynsa")
  end

  it "should reject incorrect password" do
    user = User.new
    user.password = "hackedbygchq"
    expect(user).not_to be_valid_password("hackedbynsa")
  end

  it "should have a UUID" do
    user = User.new
    expect(user.uuid).to match(/\A[0-9a-f]{8}-(?:[0-9a-f]{4}-){3}[0-9a-f]{12}\z/)
  end
end

FactoryGirl.define do
  factory :user do
    sequence :name do |n|
      "THX-%04d" % n
    end

    sequence :email_address do |n|
      "user%d@example.com" % n
    end

    password "SoSecure!11"
  end

  factory :place do
    user

    sequence :name do |n|
      "Place %d" % n
    end

    sequence :phone_number do |n|
      "+1 (555) %07d" % n
    end

    tz_name "Asia/Seoul"
  end

  factory :move do
    user
    place

    year 2013
    month 9
    mday 1
    hour 13
    minute 45
  end
end

# Stay in touch

Stay in touch makes it easy for people to get in touch with you, no matter
where you travel.

How it works:

* Set up the places you travel to, and your contact number in each.
* Add your travel itinerary by saying when you’ll arrive in a place.
* Point a Twilio phone number at your unique call flow URL.
* Phone calls follow you around the world as if by magic!

You can see the application in action at https://stay-in-touch.herokuapp.com/
